#!/usr/bin/env ruby

require 'gitlab'
require 'git'
require 'thor'

class Gittivity < Thor

  desc "issues REPO", "Tasks for a specific repo"
  def issues(path=Dir.pwd, config='config.yaml', git_loc='external', dryrun=true, group_name=nil)
   config = YAML::load_file(config)
   Gitlab.endpoint = config['gitlab'][git_loc]['endpoint']
   Gitlab.private_token = config['gitlab'][git_loc]['token']
   @gl = Gitlab.client()
   @git_path = path
   git_name = File.basename(@git_path)
   say "is #{git_name} the right repo?"
   resp = ask('Y/repo name')
   git_name = resp == 'Y' ? git_name : resp
   search = []
   begin
      search = @gl.project_search(git_name)
   rescue Exception
      puts "uh oh"
   end
   if search.length > 0
    @project = search[0]
    puts "Using #{@project.name}"
   else
     puts "Could not find project"
     Kernel.exit()
   end
   issues = @gl.issues(@project.id)
   issues.auto_paginate do |i|
     puts i.title
     more = ask "Show more? "
     if more=='n'
       break
     end
   end
  end

  no_commands do
    def get_groups()
      groups = @gl.groups()
      puts "Getting groups you are a member of..."
      if groups.length > 0
          groups.auto_paginate do |group|
            puts group
          end
      end
    end
  end
end

Gittivity.start(ARGV)
