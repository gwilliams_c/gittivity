require 'gitlab'
require 'arg-parser'
require 'git'

class Gittivity

	include ArgParser::DSL

	purpose <<-EOT
		test for arg parser
	EOT

	keyword_arg :config, 'Path to config file', short_key: '-c', default: 'config.yaml'
	keyword_arg :dryrun, 'Execute creation and modifications on github', short_key: '-d', default: true
	def initialize()
		if opts = parse_arguments
			config = YAML::load_file(opts.config)
			Gitlab.endpoint = config['gitlab']['external']['endpoint']
			Gitlab.private_token = config['gitlab']['external']['token']
			@gExternal = Gitlab.client()
			@dryrun = opts.dryrun
		end
		if @gExternal
			@working_dir = Dir.pwd
			@g = Git.open(@working_dir)
			puts "Found repo dir: " + @working_dir
			projects = Gitlab.projects(per_page: 5)
			@options = ["Get (G)roups", "Get (I)ssues", "(Q)uit"]
			user_action = get_next_action(@options)
			while user_action != 'q'
					case(user_action)
					when("G")
						get_groups()
					when('I')
						get_issues()
					end
					user_action = get_next_action(@options)
			end
		end
	end

	def get_next_action(options)
		puts "What would you like to do?"
		options.each { |option| puts option }
		return gets.chomp
	end

	def get_groups()
		groups = @gExternal.groups()
		puts "Getting groups you are a member of..."
		if groups.length > 0
				groups.auto_paginate do |group|
					puts group.name
				end
		end
	end

	def get_issues(project=nil)
		issues = @gExternal.issues(project)
		if issues.length > 0
			issues.auto_paginate do |issue|
				puts issue.title
				puts issue.project_id
				puts issue.state
				if issue.state != 'closed'
					create_branch(project, issue, 'feature')
				end
        puts "*******"
			end
		end
	end

	def create_branch(project=nil, issue=nil, type)
		if issue
			b_name = type + '/'
			b_name += issue.title.gsub! ' ', '-'
			puts b_name
			if not @dryrun
				existing_branch = @gExternal.branch(project.id, b_name)
				if not existing_branch
					branch = @gExternal.create_branch(project.id, b_name)
				end
				# TODO checkout and switch to branch created
			end
		end

	end

	def run

	end
end

Gittivity.new.run
